﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simply.Integrations.Google.Tables.Client
{
    public interface IGoogleSheetsClient
    {
        Task<IList<IList<object>>> Read(string spreadsheetId, string range);

        Task Append(string spreadsheetId, string range, List<List<object>> rows);

        Task Delete(string spreadsheetId, string range);
    }
}
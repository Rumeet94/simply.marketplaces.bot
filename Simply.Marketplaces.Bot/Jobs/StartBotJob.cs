﻿using Quartz;
using Simply.Marketplaces.Bot.Contract;

namespace Simply.Marketplaces.Bot.Jobs;

[DisallowConcurrentExecution]
public class StartBotJob : IJob
{
    private readonly ITelegramBotService _botService;

    public StartBotJob(ITelegramBotService botService)
    {
        _botService = botService;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        await _botService.StartReceiving(context.CancellationToken);
    }
}
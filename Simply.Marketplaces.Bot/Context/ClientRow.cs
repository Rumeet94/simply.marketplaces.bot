﻿namespace Simply.Marketplaces.Bot.Context;

public class ClientRow
{
    public string Id { get; }
    public string Name { get; }
    public string ChatId { get; }
    public DateTime DateCreated { get; }
    public List<ClientCredencialRow> Credencials { get; set; }

    public ClientRow(
        string id,
        string Name,
        string chatId,
        DateTime dateCreated)
    {
        Id = id;
        this.Name = Name;
        ChatId = chatId;
        DateCreated = dateCreated;
    }
}
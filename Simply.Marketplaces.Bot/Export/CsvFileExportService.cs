﻿using System.Globalization;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using Simply.Marketplaces.Bot.Contract;

namespace Simply.Marketplaces.Bot.Export;

public class CsvFileExportService : IFileExportService
{
    public async Task<byte[]> Export<T>(IEnumerable<T> records)
    {
        await using var stream = new MemoryStream();
        await using var writer = new StreamWriter(stream, Encoding.UTF8);
        await using var csvWriter = new CsvWriter(
            writer,
            new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ";"
            });

        await csvWriter.WriteRecordsAsync(records);
        await writer.FlushAsync();

        return stream.ToArray();
    }
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Simply.Marketplaces.Bot.Context;

namespace Simply.Marketplaces.Bot;

public class ClientsService
{
    private readonly BotDbContext _dbContext;

    public ClientsService(IServiceProvider serviceProvider)
    {
        var scope = serviceProvider.CreateScope();

        _dbContext = scope.ServiceProvider.GetRequiredService<BotDbContext>();
    }

    public async Task<ClientRow> Get(string chatId)
    {
        var clientRow = await _dbContext.Clients
            .AsNoTracking()
            .AsSingleQuery()
            .Include(p => p.Credencials)
            .SingleOrDefaultAsync(p => p.ChatId == chatId);

        if (clientRow == null) throw new InvalidOperationException("The client is not found");

        return clientRow;
    }

    public async Task<ICollection<ClientRow>> GetAll()
    {
        return await _dbContext.Clients
            .AsNoTracking()
            .AsSingleQuery()
            .Include(p => p.Credencials)
            .ToListAsync();
    }
}
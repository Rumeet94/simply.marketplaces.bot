﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;

namespace Simply.Integrations.Google.Tables.Client
{
    public class GoogleSheetsClient : IGoogleSheetsClient
    {
        private static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        private static readonly SheetsService Service;

        static GoogleSheetsClient()
        {
            try
            {
                using var stream = new FileStream("clients-secrets.json", FileMode.Open, FileAccess.Read);

                var credential = GoogleCredential
                    .FromStream(stream)
                    .CreateScoped(Scopes);

                Service = new SheetsService(
                    new BaseClientService.Initializer
                    {
                        HttpClientInitializer = credential, ApplicationName = "WbStatistic"
                    });

                Service.HttpClient.Timeout = new TimeSpan(
                    0,
                    0,
                    0,
                    200);
            }
            catch (Exception e)
            {
            }
        }

        public async Task Delete(string spreadsheetId, string range)
        {
            var request = Service.Spreadsheets.Values.Clear(
                new ClearValuesRequest(),
                spreadsheetId,
                range);

            await request.ExecuteAsync();
        }

        public async Task<IList<IList<object>>> Read(string spreadsheetId, string range)
        {
            var request = Service.Spreadsheets.Values.Get(spreadsheetId, range);
            var responce = await request.ExecuteAsync();

            return responce.Values;
        }

        public async Task Append(
            string spreadsheetId,
            string range,
            List<List<object>> rows)
        {
            var values = new List<IList<object>>();

            values.AddRange(rows);

            var valueRange = new ValueRange { Values = values };

            var request = Service.Spreadsheets.Values.Append(
                valueRange,
                spreadsheetId,
                range);

            request.ValueInputOption =
                SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;

            await request.ExecuteAsync();
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;

namespace Simply.Marketplaces.Bot.Migrations
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var dbContext = new DesignTimeDbContextFactory().CreateDbContext();

            var pending = await dbContext.Database.GetPendingMigrationsAsync().ConfigureAwait(false);

            Console.WriteLine($"Pending migrations on {dbContext.GetType().Name} ({dbContext.Database.ProviderName}):");

            foreach (var m in pending)
            {
                Console.WriteLine($"{m}");
            }

            Console.WriteLine("Migrate...");
            await dbContext.Database.MigrateAsync().ConfigureAwait(false);
            Console.WriteLine("Migration done.");
        }
    }
}
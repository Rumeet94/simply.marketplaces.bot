﻿using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using CsvHelper.Configuration.Attributes;
using Quartz;
using Simply.Integrations.Google.Tables.Client;
using Simply.Marketplaces.Bot.Context;
using Simply.Marketplaces.Bot.Contract;

namespace Simply.Marketplaces.Bot.Jobs;

[DisallowConcurrentExecution]
public class CheckStockRemaindersJob : IJob
{
    private static string SheetRange = "Склад!B7:W";

    private readonly ITelegramBotService _botService;
    private readonly IGoogleSheetsClient _googleSheetsClient;
    private readonly IFileExportService _fileExportService;
    private readonly ClientsService _clientsService;

    public CheckStockRemaindersJob(
        ITelegramBotService botService,
        IGoogleSheetsClient googleSheetsClient,
        IFileExportService fileExportService,
        ClientsService clientsService)
    {
        _botService = botService;
        _googleSheetsClient = googleSheetsClient;
        _fileExportService = fileExportService;
        _clientsService = clientsService;
    }

    public async Task Execute(IJobExecutionContext context)
    {
        var clients = await _clientsService.GetAll();
        if (!clients.Any())
        {
            return;
        }

        foreach (var client in clients)
        {
            try
            {
                if (!client.Credencials.Any())
                {
                    continue;
                }

                var records = await CreateRecords(client.Credencials);
                var report = await _fileExportService.Export(records);

                await _botService.SendFileMessage(
                    long.Parse(client.ChatId),
                    "Необходимо пополнить товары",
                    $"Критические остатки {DateTime.Now}.csv",
                    report,
                    context.CancellationToken);
            }
            catch (Exception ex)
            {
                continue;
            }
        }
    }

    private async Task<List<Remainder>> CreateRecords(List<ClientCredencialRow> credencials)
    {
        var list = new List<Remainder>();
        foreach (var item in credencials)
        {
            try
            {
                var sheetData = await _googleSheetsClient.Read(
                    item.Value,
                    SheetRange);

                if (!sheetData.Any())
                {
                    continue;
                }

                var remainders = sheetData
                    .Where(s => s.Count == 22)
                    .Select(s => new Remainder(
                        s[0].ToString(),
                        s[1].ToString(),
                        s[2].ToString(),
                        s[3].ToString(),
                        s[21].ToString()));

                list.AddRange(remainders);
            }
            catch (Exception e)
            {
            }
        }

        return list;
    }
}

internal record Remainder(
    [Display(Name = "Бренд")]
    string BrandName,
    [Display(Name = "ШК")]
    string VendorCode,
    [Display(Name = "Артикул ВБ")]
    string Barcode,
    [Display(Name = "Размер")]
    string TechSize,
    [Display(Name = "Дней до заказа")]
    string BuyDayCount);
﻿using Microsoft.EntityFrameworkCore;

namespace Simply.Marketplaces.Bot.Context;

public class BotDbContext : DbContext
{
    public DbSet<ClientRow> Clients { get; set; }

    public DbSet<ClientCredencialRow> Credencials { get; set; }

    public BotDbContext(DbContextOptions<BotDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        AddClients(modelBuilder);
        AddClientCredencials(modelBuilder);
    }

    private static void AddClients(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<ClientRow>()
            .HasKey(x => x.Id);

        modelBuilder
            .Entity<ClientRow>()
            .Property(x => x.Name);

        modelBuilder
            .Entity<ClientRow>()
            .Property(x => x.ChatId);

        modelBuilder
            .Entity<ClientRow>()
            .Property(x => x.DateCreated);
    }

    private static void AddClientCredencials(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<ClientCredencialRow>()
            .HasKey(x => x.Id);

        modelBuilder
            .Entity<ClientCredencialRow>()
            .Property(x => x.ClientId);

        modelBuilder
            .Entity<ClientCredencialRow>()
            .Property(x => x.Type);

        modelBuilder
            .Entity<ClientCredencialRow>()
            .Property(x => x.Value);

        modelBuilder
            .Entity<ClientCredencialRow>()
            .HasOne(p => p.Client)
            .WithMany(p => p.Credencials)
            .HasForeignKey(p => p.ClientId);
    }
}
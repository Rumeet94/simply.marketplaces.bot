﻿namespace Simply.Marketplaces.Bot.Contract;

public interface IFileExportService
{
    Task<byte[]> Export<T>(IEnumerable<T> records);
}
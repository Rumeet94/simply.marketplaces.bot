﻿using Telegram.Bot.Types;

namespace Simply.Marketplaces.Bot.Bot;

internal class TelegramBotMessageHandler
{
    private readonly ClientsService _service;

    public TelegramBotMessageHandler(ClientsService service)
    {
        _service = service;
    }

    public async Task<string> CreateAnswer(Message income)
    {
        try
        {
            await _service.Get(income.Chat.Id.ToString());

            return "Формируем для Вас отчетность каждый день в 10.00 по мск.";
        }
        catch (Exception ex)
        {
            return "Я - бот сервиса МаркетЦифра. Для начала работы необходимо обратиться к @aydar_rafikoff";
        }
    }
}
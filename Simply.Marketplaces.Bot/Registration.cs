﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Simply.Integrations.Google.Tables;
using Simply.Marketplaces.Bot.Bot;
using Simply.Marketplaces.Bot.Context;
using Simply.Marketplaces.Bot.Contract;
using Simply.Marketplaces.Bot.Export;
using Simply.Marketplaces.Bot.Jobs;
using Telegram.Bot;

namespace Simply.Marketplaces.Bot
{
   public static class Registration
    {
        public static void AddBotSettings(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddGoogleSheets(configuration);
            services.AddTransient<IFileExportService, CsvFileExportService>();

            AddDatabase(services);
            AddTelegramBot(services);
            AddQuartzJob(services);
        }

        public static void AddDatabase(IServiceCollection services)
        {
            services.AddDbContextPool<BotDbContext>(
                (_, b) =>
                    b.UseNpgsql("Server=localhost;Port=5432;Database=Bot;User Id=postgres;Password=personal9460;"));
            services.AddScoped<ClientsService>();
        }

        public static void AddTelegramBot(IServiceCollection services)
        {
            services
                .AddSingleton<ITelegramBotClient, TelegramBotClient>(_ => new TelegramBotClient("5133414221:AAFuvi5k5r2euXTWPg1iSfpXmH0Hghf4KwI"))
                .AddScoped<ITelegramBotService, TelegramBotService>()
                .AddScoped<TelegramBotMessageHandler>();
        }

        public static void AddQuartzJob(IServiceCollection services)
        {
            services.AddQuartz(
                q =>
                {
                    q.UseMicrosoftDependencyInjectionJobFactory();

                    q.AddJob<StartBotJob>(opts => opts.WithIdentity(new JobKey(nameof(StartBotJob))));
                    q.AddTrigger(opts => opts
                        .ForJob(nameof(StartBotJob))
                        .WithIdentity(nameof(StartBotJob) + "-trigger")
                        .StartNow());

                    q.AddJob<CheckStockRemaindersJob>(opts => opts.WithIdentity(new JobKey(nameof(CheckStockRemaindersJob))));
                    q.AddTrigger(opts => opts
                        .ForJob(nameof(CheckStockRemaindersJob))
                        .WithIdentity(nameof(CheckStockRemaindersJob) + "-trigger")
                        .StartNow());
                });

            services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);
        }
    }
}
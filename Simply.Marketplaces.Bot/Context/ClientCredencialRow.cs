﻿namespace Simply.Marketplaces.Bot.Context;

public class ClientCredencialRow
{
    public string Id { get; }
    public string ClientId { get; }
    public string Type { get; }
    public string Value { get; }
    public ClientRow Client { get; }

    public ClientCredencialRow(
        string id,
        string clientId,
        string type,
        string value)
    {
        Id = id;
        ClientId = clientId;
        Type = type;
        Value = value;
    }
}
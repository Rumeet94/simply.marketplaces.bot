﻿namespace Simply.Marketplaces.Bot.Contract;

public interface ITelegramBotService
{
    Task StartReceiving(CancellationToken cancellationToken = default);

    Task SendTextMessage(
        long chatId,
        string text,
        CancellationToken cancellationToken = default);

    Task SendFileMessage(
        long chatId,
        string caption,
        string filename,
        byte[] content,
        CancellationToken cancellationToken = default);
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

using Simply.Marketplaces.Bot.Context;

namespace Simply.Marketplaces.Bot.Migrations
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<BotDbContext>
    {
        public BotDbContext CreateDbContext(params string[] args)
        {
            //var config = new ConfigurationBuilder()
            //    .AddJsonFile("appsettings.json")
            //    .AddEnvironmentVariables()
            //    .Build();

            var optionsBuilder = new DbContextOptionsBuilder<BotDbContext>();
            optionsBuilder.UseNpgsql(
                "Server=localhost;Port=5432;Database=Bot;User Id=postgres;Password=123123123;",
                //config.GetConnectionString("PostgreSQL:BotDb"),
                builder => builder.MigrationsAssembly(typeof(DesignTimeDbContextFactory).Assembly.GetName().Name));

            return new BotDbContext(optionsBuilder.Options);
        }
    }
}
﻿using System.Collections.Generic;

namespace Simply.Integrations.Google.Tables
{
    public class GoogleSheetsSettings
    {
        public string ClientSpreadsheetId { get; init; }

        public TemplateSpreadsheet Template { get; init; }
    }

    public class TemplateSpreadsheet
    {
        public string SpreadsheetId { get; init; }

        public List<int> SheetsIds { get; set; }
    }
}
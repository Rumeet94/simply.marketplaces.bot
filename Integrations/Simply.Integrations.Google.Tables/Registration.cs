﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Simply.Integrations.Google.Tables.Client;

namespace Simply.Integrations.Google.Tables
{
    public static class Registration
    {
        public static IServiceCollection AddGoogleSheets(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IGoogleSheetsClient, GoogleSheetsClient>();

            return services;
        }
    }
}
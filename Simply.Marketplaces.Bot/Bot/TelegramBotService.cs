﻿using Simply.Marketplaces.Bot.Contract;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;

namespace Simply.Marketplaces.Bot.Bot;

internal class TelegramBotService : ITelegramBotService
{
    private readonly ITelegramBotClient _client;
    private readonly TelegramBotMessageHandler _messageHandler;

    public TelegramBotService(
        ITelegramBotClient client,
        TelegramBotMessageHandler messageHandler)
    {
        _client = client;
        _messageHandler = messageHandler;
    }

    public async Task StartReceiving(CancellationToken cancellationToken = default)
    {
        await _client.SetWebhookAsync(
            string.Empty,
            cancellationToken: cancellationToken);

        _client.StartReceiving(
            HandleUpdateAsync,
            HandleErrorAsync,
            new ReceiverOptions(),
            cancellationToken);
    }

    public async Task SendTextMessage(
        long chatId,
        string text,
        CancellationToken cancellationToken = default)
    {
        await _client.SendTextMessageAsync(
            chatId,
            text,
            cancellationToken: cancellationToken);
    }

    public async Task SendFileMessage(
        long chatId,
        string caption,
        string filename,
        byte[] content,
        CancellationToken cancellationToken = default)
    {
        await _client.SendDocumentAsync(
            chatId,
            new InputOnlineFile(
                new MemoryStream(content),
                filename),
            caption: caption,
            cancellationToken: cancellationToken);
    }

    private async Task HandleUpdateAsync(
        ITelegramBotClient botClient,
        Update update,
        CancellationToken cancellationToken)
    {
        if (update.Type == UpdateType.ChatJoinRequest)
        {
            await botClient.DeleteMessageAsync(
                update.Message.Chat.Id,
                update.Message.MessageId);
        }

        if (update.Message is { } message)
        {
            if (message.Type == MessageType.ChatMembersAdded)
            {
                await botClient.DeleteMessageAsync(
                    message.Chat.Id,
                    message.MessageId);
            }

            //var text = await _messageHandler.CreateAnswer(message);

            //await botClient.SendTextMessageAsync(
            //    message.Chat,
            //    text,
            //    cancellationToken: cancellationToken);
        }
    }

    private async Task HandleErrorAsync(
        ITelegramBotClient botClient,
        Exception exception,
        CancellationToken cancellationToken)
    {
        if (exception is ApiRequestException apiRequestException)
        {
            //await botClient.SendTextMessageAsync(123, apiRequestException.ToString());
        }
    }
}